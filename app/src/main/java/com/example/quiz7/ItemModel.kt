package com.example.quiz7

class ItemModel(
    val fieldID: Int,
    val hint: String,
    val field_type: String,
    val keyboard: String,
    val required: Boolean,
    val is_active: Boolean,
    val icon: String

) {
    var fieldInfo: String = ""
}
