package com.example.quiz7

import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.choose_field_recyclerview_layout.view.*
import kotlinx.android.synthetic.main.inputfield_recyclerview_layout.view.*

class FieldsRecyclerViewAdapter(
    private val fields: Array<ItemModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val INPUT_TYPE = 1
        const val CHOOSE_TYPE = 2
    }


    inner class InputViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            val model = fields[adapterPosition]
            itemView.fieldEditText.hint = model.hint

            if (model.is_active) {
                itemView.fieldEditText.visibility = View.VISIBLE
            } else {
                itemView.fieldEditText.visibility = View.GONE
            }

            Glide.with(itemView.context).load(model.icon).centerCrop()
                .into(object : CustomTarget<Drawable>() {
                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        itemView.fieldEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            resource,
                            null
                        )
                    }


                })
            itemView.fieldEditText.addTextChangedListener(textWatcher)

        }

        private val textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                fields[adapterPosition].fieldInfo = s.toString()

            }

        }
    }

    inner class ChooseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val model = fields[adapterPosition]
            itemView.fieldButton.text = model.hint

            if (model.is_active) {
                itemView.fieldButton.visibility = View.VISIBLE
            } else {
                itemView.fieldButton.visibility = View.GONE
            }

            Glide.with(itemView.context).load(model.icon).centerCrop()
                .into(object : CustomTarget<Drawable>() {
                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        itemView.fieldButton.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            resource,
                            null
                        )
                    }


                })

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == INPUT_TYPE) {
            InputViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.inputfield_recyclerview_layout,
                    parent,
                    false
                )
            )
        } else {
            ChooseViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.choose_field_recyclerview_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount() = fields.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is InputViewHolder)
            holder.onBind()
        else if (holder is ChooseViewHolder)
            holder.onBind()


    }

    override fun getItemViewType(position: Int): Int {
        val model = fields[position]
        return if (model.field_type == "input") {
            INPUT_TYPE
        } else {
            CHOOSE_TYPE
        }
    }
}